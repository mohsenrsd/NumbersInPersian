/*
 ============================================================================
 Name        : NumbersInPersian.c
 Author      : Mohsen Rashidi
 website     : Codeblog.ir
 email	     : mohsenrsd@gmail.com
 Version     : 1.0.0
 Description : convert Numbers to persian sentences
 ============================================================================
 */


/*  --------------------< Include headers >--------------------------*/

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <ctype.h>
	#include <math.h>
	#include "NumbersInPersian.h"	
	#include "fingilishIndexes.h"

/*  ------------------------------------------------------------------*/

char* constantNumbersStrings[] ={

		"SEFR"			, // 0
 		"YEK"			, // 1
 		"DO"			, // 2
 		"SE"			, // 3
 		"CHHAR"			, // 4
 		"PANJ"			, // 5
 		"SHESH"			, // 6
 		"HAFT"			, // 7
 		"HASHT"			, // 8
 		"NOH"			, // 9
 		"DAH"			, // 10
 		"YAZDAH"		, // 11
 		"DAVAZDAH"		, // 12
 		"SIZDAH"		, // 13
 		"CHHARDAH"		, // 14
 		"PAANZDAH"		, // 15
 		"SHAANZDAH"		, // 16
 		"HEFDAH"		, // 17
 		"HEJDAH"		, // 18
 		"NUZDAH"		, // 19
 		"BIST"			, // 20
 		"SI"			, // 21 30
 		"CHEHEL"		, // 22 40
 		"PANJAH"		, // 23 50
 		"SHAST"			, // 24 60
 		"HAFTAAD"		, // 25 70
 		"HASHTAAD"		, // 26 80
 		"NAVAD"			, // 27
 		"SAD"			, // 28 100
 		"DIVIST"		, // 29 200
 		"SISAD"			, // 30 300
 		"CHHAARSAD"		, // 31 400
 		"PAANSAD"		, // 32 500
 		"SHESHSAD"		, // 33 600
 		"HAFTSAD"		, // 34 700
 		"HASHTSAD"		, // 35 800
 		"NOHSAD"		, // 36 900
 		"HEZAR"			, // 37 1,000
 		"MILIYUN"		, // 38 1,000,000
 		"MILIYARD"		, // 39 1,000,000,000
 		"BILIYUN"		, // 40 1,000,000,000,000
 		"BILIYARD"		, // 41 1,000,000,000,000,000
		"TIRILIUN"		, // 42 1,000,000,000,000,000,000
 		"TIRILIYAARD" 	, // 43 1,000,000,000,000,000,000,000
		"NOtDEFINED"		, // 44
};

int output(char* str){
	return printf("%s",str);
}
BOOL strIsDigit(const char* str){

	int i = 0;

	if(!str)
		return FALSE;
	for( i = 0 ; str[i] ; i++){
		if(!isdigit(str[i]))
			return FALSE;
	}
	return TRUE;

}

void digitGroup(char* str, char* output){
	int x = strlen(str);
	strcpy(output, str);
	while( (x-=3) > 0 ){
		memmove(output + x + 1, output + x, strlen(output) - x + 1);
		output[x] = ',';
	}
}


char* deleteZero(const char *inputStr){
	int i = 0;

	if(!(*inputStr))
		return NULL;

	char* result = NULL;
	while (*(result = ((char*)inputStr + i++)) == '0');

	if(!(*result))
		return result - 1;

	return result;
}

int16 checkFormatValidity(const char* str){
	if(strIsDigit(str) != TRUE)
		return FALSE;
	return TRUE;
}

char* editFormat(const char* str){
	return deleteZero(str);
}

char* getStairStr(const uint stair){

	switch(stair){

	case 1:
		return NULL;
	case 2:
		return constantNumbersStrings[HEZARHA];
		break;
	case 3:
		return constantNumbersStrings[MILIYUNHA];
		break;
	case 4:
		return constantNumbersStrings[MILIYARDHA];
		break;
	case 5:
		return constantNumbersStrings[BILIYUNHA];
		break;
	case 6:
		return constantNumbersStrings[BILIYARDHA];
		break;
	case 7:
		return constantNumbersStrings[TIRILIUNHA];
		break;
	case 8:
		return constantNumbersStrings[TIRILIYAARDHA];
		break;
	default:
		return constantNumbersStrings[NODEFINE];
		break;
	}
	return NULL;
}


char* decimal_OR_Centesimal (char ch, int stair){

	char* buffStr = NULL;

	switch(ch){
	case '1':
		buffStr = (stair == CENTESIMAL) ? constantNumbersStrings[SAD] : NULL;
		break;
	case '2':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[BIST] : constantNumbersStrings[DEVIST];
		break;
	case '3':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[SI] : constantNumbersStrings[SISAD];
		break;
	case '4':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[CHEHEL] : constantNumbersStrings[CHAHARSAD];
		break;
	case '5':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[PANJAH] : constantNumbersStrings[PAANSAD];
		break;
	case '6':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[SHAST] : constantNumbersStrings[SHESHSAD];
		break;
	case '7':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[HAFTAAD] : constantNumbersStrings[HAFTSAD];
		break;
	case '8':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[HASHTAAD] : constantNumbersStrings[HASHTSAD];
		break;
	case '9':
		buffStr = (stair == DECIMAL) ? constantNumbersStrings[NAVAD] : constantNumbersStrings[NOHSAD];
		break;
	default:
		break;
	}

	return buffStr;
}

void printTriliteral(const char* theTriliteralStr){

	int digit = atoi(theTriliteralStr);
	char* triliteralStr;

	triliteralStr = deleteZero(theTriliteralStr);

	if( digit <= 20 ){
		output(constantNumbersStrings[digit]);
	}

	else if( (digit < 100) && (digit > 20) ){

		output( decimal_OR_Centesimal(triliteralStr[0], DECIMAL) );

		if( digit%10 != 0){
			output( SEPERATOR);
			output( constantNumbersStrings[digit%10] );
		}
	}

	else{ //  1000 > digit >= 100
		int temp;
		output( decimal_OR_Centesimal(triliteralStr[0], CENTESIMAL) );
		temp = atoi(&triliteralStr[1]);

		if( temp > 0 ){

			if( temp <= 20 ){
			output( SEPERATOR);
			output(constantNumbersStrings[digit%100]);
			}
			else {
				output( SEPERATOR);
				output( decimal_OR_Centesimal(triliteralStr[1], DECIMAL) );
				if(triliteralStr[2] != '0'){
					output( SEPERATOR);
					output( constantNumbersStrings[digit%10] );
				}
			}
		}
	}
}

int16 convertToPersianSentence(const char* digitStr){

	uint stage = 0;
	uint stair = 0;
	uint digitStrLen = 0;
	char triliteralBuf[3+1] = {0};
	char* editedStrDigit;
	char* temp = 0;

	if( checkFormatValidity(digitStr) != TRUE )
		return FALSE;

	editedStrDigit = editFormat(digitStr);

	digitStrLen = strlen(editedStrDigit);

	if( digitStrLen == 0 )
		return FALSE;


	stage = ( (digitStrLen % 3) == 0 ) ? 3 : (digitStrLen % 3);
	stair = ceil( (float)digitStrLen / 3.f );

	strncpy(triliteralBuf, editedStrDigit, stage);

	printTriliteral(triliteralBuf);

	temp = getStairStr(stair--);

	if(temp){
		output(SPACE);
		output( temp );
	}


	while( stage < digitStrLen ) {

		strncpy(triliteralBuf, editedStrDigit + stage , 3);
		if( atoi(triliteralBuf) > 0 ){
			output( SEPERATOR );
			printTriliteral(triliteralBuf);
			temp = getStairStr(stair);
			if(temp){
				output(SPACE);
				output( temp );
			}

		}
		stage += 3;
		stair--;
	}

	return TRUE;

}


