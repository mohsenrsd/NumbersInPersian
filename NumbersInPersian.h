/*
 ============================================================================
 Name        : NumbersInPersian.h
 Author      : Mohsen Rashidi
 website     : Codeblog.ir
 email	     : mohsenrsd@gmail.com
 Version     : 1.0.0
 Description : convert Numbers to persian sentences
 ============================================================================
 */

/*  --------------------< Include headers >--------------------------*/

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <ctype.h>
	#include <math.h>
	#include "fingilishIndexes.h"

/*  ------------------------------------------------------------------*/
/*  --------------------< Macro Definition >--------------------------*/

	#define TRUE			(char)0
	#define FALSE			(char)-1
	#define DECIMAL 		0
	#define CENTESIMAL		1
	#define SPACE	  		" "
	#define	SEPERATOR 		" 'O "

/*  ------------------------------------------------------------------*/
/*  --------------------< Type Definition >--------------------------*/

	typedef int short		int16;
	typedef unsigned int 		uint;
	typedef char 			BOOL;

/*  ------------------------------------------------------------------*/

int output(char* str);
BOOL strIsDigit(const char* str);
void digitGroup(char* str, char* output);
char* deleteZero(const char *inputStr);
int16 checkFormatValidity(const char* str);
char* editFormat(const char* str);
char* getStairStr(const uint stair);
char* decimal_OR_Centesimal (char ch, int stair);
void printTriliteral(const char* theTriliteralStr);
int16 convertToPersianSentence(const char* digitStr);

