#ifndef	FINGILISH_INDEXES
#define FINGILISH_INDEXES


#define		YEK			1
#define		DO			2
#define		SE			3
#define		CHHAR			4
#define		PANJ			5
#define		SHESH			6
#define		HAFT			7
#define		HASHT			8
#define		NOH			9
#define		DAH			10
#define		YAZDAH			11
#define		DAVAZDAH		12
#define		SIZDAH			13
#define		CHHARDAH		14
#define		PAANZDAH		15
#define		SHAANZDAH		16
#define		HEFDAH			17
#define		HEJDAH			18
#define		NUZDAH			19
#define		BIST			20
#define		SI			21
#define		CHEHEL			22
#define		PANJAH			23
#define		SHAST			24
#define		HAFTAAD			25
#define		HASHTAAD		26
#define		NAVAD			27
#define		SAD			28
#define		DEVIST			29
#define		SISAD			30
#define		CHAHARSAD		31
#define		PAANSAD			32
#define		SHESHSAD		33
#define		HAFTSAD			34
#define		HASHTSAD		35
#define		NOHSAD			36
#define		HEZARHA			37
#define		MILIYUNHA		38
#define		MILIYARDHA		39
#define		BILIYUNHA		38
#define		BILIYARDHA		39
#define		TIRILIUNHA		40
#define		TIRILIYAARDHA		41
#define		NODEFINE		67

#endif //FINGILISH_INDEXES

