#include <stdio.h>
#include "NumbersInPersian.h"

int main(void) {
	char str[200];
	char digitGroupedStr[1000];

	while(1){
		printf("/===============================\n");
		printf("\nEnter a number: ");
		scanf("%200s", str);
		digitGroup(str, digitGroupedStr);
		printf("\n%s\n\n", digitGroupedStr);
		convertToPersianSentence(str);
		printf("\n\n");
	}

	return 0;
}
